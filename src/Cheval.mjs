//on importe la class Animal 
import {Animal} from "./Animal.mjs"
//on crée une class Cheval qui est une extension de la class Animal
//export permet de recup Cheval ailleur avec import
export class Cheval extends Animal{
    //constructeur du Cheval
    constructor(nom){
        //la methode super() fait apel au constructeur du parent Animal
        super(nom, "huhuhu");
    }
    //fonction qui affiche le nom du cheval + je galope
    galop(){
        console.log(this.nom + " : " + "je galope");
    }
}