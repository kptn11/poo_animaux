import {Animal} from "./Animal.mjs"

export class Pokemon extends Animal{
    constructor(nom, cri){
        super(nom, cri);
    }
}