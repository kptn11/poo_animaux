//decommenter les Log pour afficher les results dans la console
// dans le terminal :
    //node src/test.mjs    ---->   permet de lancer le test
    //npm start        -----> permet de lancer le serveur

//importer chaque objet
import {Animal} from "./Animal.mjs"
import {Vache} from "./Vache.mjs"
import {Cheval} from "./Cheval.mjs"
import {Pokemon} from "./Pokemon.mjs"
import {Enclos} from "./Enclos.mjs"
//________________________________________________________________
//créer un tableau des enclos
let allEnclos = []
//créer une vache marguerite
let marguerite = new Vache("marguerite")
// console.log(marguerite)
//bruit de la vache 
marguerite.parle()
//fonction pour créer une vache
function createVache(nom) {
    let newVache = new Vache(nom)
    enclos1.put(newVache)
}
//créer un cheval boby
let boby = new Cheval("boby")
// console.log(boby)
boby.parle()

//créer un pokemon pikachu
let pikachu = new Pokemon("pikachu", "Pika pika ! ")
// console.log(pikachu)
pikachu.parle()
//créer un 1st enclos 
let enclos1 = new Enclos("étable")
allEnclos.push(enclos1)
//on met boby le Cheval dans l'enclo1
enclos1.put(boby)
//on créer i * new Cheval et on les met dans l'enclo1
for(let i = 1; i <3; i++){
    let unNouveauCheval = new Cheval('cheval : '+i)
    enclos1.put(unNouveauCheval)
}
// console.log(enclos1)
//on crée un 2nd enclo
let enclos2 = new Enclos("prairie")
allEnclos.push(enclos2)
//on met boby le Cheval dans l'enclo 2
enclos2.put(boby)
// console.log(enclos1)
// console.log(enclos2)
//on crée un 3rd enclo pokeball
let pokeball = new Enclos("pokeball")
allEnclos.push(pokeball)
//on met pikachu dans sa pokeball
pokeball.put(pikachu)
// console.log(pokeball.animaux)
//on crée un 4th enclo arène
let arène = new Enclos("arène")
allEnclos.push(arène)
//on met pikachu dans l'arène
arène.put(pikachu)
console.log(arène.animaux)
console.log(pokeball.animaux)
console.log(allEnclos)
allEnclos.forEach(Enclos => {
    Enclos.animaux.forEach(animal => {
      delete animal.Enclos
    })
  })
// Importe le module fs (système de fichiers) de Node.js
import fs from 'fs'
// Crée un objet vide
const data = {}
// Convertit l'objet en format JSON (une chaîne de caractères)
const jsonData = JSON.stringify(data)
// Écrit le contenu JSON dans le fichier Enclos.json
fs.writeFileSync('Enclos.json', jsonData)
// Convertit l'objet allEnclos en format JSON (une chaîne de caractères)
const enclosJSON = JSON.stringify(allEnclos)
// Écrit le contenu JSON dans le fichier Enclos.json, en remplaçant le contenu précédent
fs.writeFile('Enclos.json', enclosJSON, function(err) {
  if (err) {
    // Si une erreur se produit, affiche un message d'erreur
    console.log('Erreur lors de l\'écriture du fichier : ' + err)
  } else {
    // Si l'écriture est réussie, affiche un message de confirmation
    console.log('Le fichier a été enregistré avec succès.')
  }
})








