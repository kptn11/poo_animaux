//on importe la class Animal 
import {Animal} from "./Animal.mjs";
//on crée une class Vache qui est une extension de la class Animal
//export permet de recup Vache ailleur avec import
export class Vache extends Animal{
    //constructeur de la Vache
    constructor(nom){
        //la methode super() fait apel au constructeur du parent Animal
        super(nom, "Meuuh");
    }
    //fonction qui affiche le nom du cheval + je galope
    degasage(){
        console.log(this.nom + " prout !");
    }
}
