//on crée une class animal
//export permet de recup Animal ailleur avec import 
export class Animal{
    //on donne tout les attributs de l'animal a notre constructor 
    //afin de construire cet animal 
    constructor(nom, cri){
        this.nom = nom;
        this.cri = cri;
        this.enclos = {};
    }
    //fonction qui affiche dans la console le nom et le cri de l'animal
    parle(){
        console.log(this.nom + " : " + this.cri);
    }
}
