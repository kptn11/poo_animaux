//on crée une class Enclos
//export permet de recup Enclos ailleur avec import
export class Enclos{

    constructor(nom){
        this.animaux = [] //tableau representant l'enclo
        this.limit = 10 // limite d'animal dans l'enclo
        this.nom = nom//nom de l'enclos
    };
    put(animal){//mettre un animal dans l'enclo
        if(this.animaux.length < this.limit){//si le nombre d'animal est inferieur a la limite
            if(animal.Enclos === this){// pour eviter les doublons
                console.log(animal.nom + " est déjà dans cet enclo");
                return;
            }
            // pour contrôler si l'animal doit être remove avant de l'add
            if(animal.Enclos)//verifie si l'animal est dans un enclo 
            animal.Enclos.remove(animal);//si il l'est on le supprime de cet enclo
            animal.Enclos = this;//et on le met dans l'enclo désiré
            this.animaux.push(animal);// met l'animal dans le tab animaux
            console.log('on met ' + animal.nom + ' dans un enclos')
        } else{//sinon
            console.log('Y a plus de place!!!');
        }
    }
    remove(animal){  // supprimer un animal d'un enclos
        let index = this.animaux.indexOf(animal);// On récupère l'index de l'animal à supprimer dans le tableau animaux
        this.animaux.splice(index, 1);// On utilise la méthode splice pour supprimer l'animal avec l'index trouvé, et on supprime un élément (dans ce cas, l'animal en question)
        animal.Enclos = undefined ; // On met à jour la propriété Enclos de l'animal pour le retirer de l'enclos
        console.log("on enlève " + animal.nom + " de l'enclos");// On affiche un message dans la console pour indiquer que l'animal a bien été retiré de l'enclos
    }

}